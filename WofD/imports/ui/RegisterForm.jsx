import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Input from './components/Input';


//TODO: https://codechron.gitbooks.io/intro-to-meteor-with-react/content/routing_and_main_app_views.html
// https://react.rocks/example/React_Signup_Form
// http://lorenstewart.me/2016/10/31/react-js-forms-controlled-components/

export default class RegisterForm extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      userName: '',
      password: '',
      password_confirm: ''

    };

    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.clearForm = this.clearForm.bind(this);
  }

  handleUserNameChange(event)
  {
    this.setState({ userName: event.target.value }, () => console.log('name:', this.state.userName));
  }

  handlePassword(event)
  {
    event.preventDefault();

  }

  handleSubmit(event)
  {
    alert('user name is ' + this.state.userName);
    event.preventDefault();
  }

  clearForm(event)
  {
    event.preventDefault();
    this.setState({
      userName: '',
      password: '',
      password_confirm: ''
    });
  }


  render()
  {
    return(
      <form onSubmit={this.handleSubmit}>
        <Input
            inputType={'text'}
            title={'User Name'}
            name={'username'}
            controlFunc={this.handleUserNameChange}
            content={this.state.userName}
            placeholder={'User Name'}/>
        <Input
            inputType={'password'}
            title={'Create Password'}
            name={'password'}
            controlFunc={this.handlePassword}
            content={this.state.password}
            placeholder={'Password'}/>
        <Input
            inputType={'password'}
            title={'Confirm Password'}
            name={'password_confirm'}
            controlFunc={this.handleUserNameChange}
            content={this.state.password_confirm}
            placeholder={'Confirm Password'}/>
        <input
            type="submit"
            className="btn btn-primary float-right"
            value="Submit"/>
        <button
            className="btn btn-link float-left"
            onClick={this.clearForm}>Clear form</button>
      </form>

    );
  }
    

}
