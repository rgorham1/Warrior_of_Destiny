import React, { Component } from 'react';

let buttonStyle = {
  margin: '10px 10px 10px 0'
};

export default class Button extends Component
{
  constructor(props)
  {
    super();

  }

  render()
  {
    return(
      <div>
        <button
          className="btn btn-primary btn-lg"
          id={this.props.id}
          style={buttonStyle}
          onClick={this.props.handleClick}>{this.props.label}
        </button>
      </div>
    );
  }

}
