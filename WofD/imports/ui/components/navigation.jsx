import React from 'react';
import { IndexLink, Link } from 'react-router-dom';

export const Navigation = () => (
  <ul>
    <li><IndexLink to="/" activeClassName="active">Index</IndexLink></li>
  </ul>
)
