import React, { Component } from 'react';
import Button from './Button';

// App component - represents the whole app
export default class App extends Component
{
  constructor(props)
  {
    super();

    this.handleClick = this.handleClick.bind(this);
  }


  handleClick()
  {
    alert('Button clicked');
  }



  render() {
    return (
      <div className="container">
        <header>
          <h1 id = 'header'>Warrior of Destiny</h1>
        </header>

        <Button
          id='start'
          handleClick={this.handleClick}
          label='Join the Warrior Ranks'/>

        <Button
          id='continue'
          handleClick={this.handleClick}
          label='Continue Saved Game'/>

        <Button
          id='help'
          handleClick={this.handleClick}
          label='Help'/>

      </div>
    );
  }
}
