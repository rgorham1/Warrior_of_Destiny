import Character from './Character';

export default class PlayableCharacter extends Character
{
	constructor(userName, characterLevel = 1, curXp = 1)
	{
		super();
		this.userName = userName;
		this.level = characterLevel;
		this.hitPoints = null;
		this.maxHitPoints = null;
		this.attackPoints = null;
		this.maxAttackPoints = null;
		this.xp = curXp;
		this.aliveStatus = true;
		this.inventory = [null, null, null, null];
		this.attackList = null;
		this.attrs = null;

	}


}