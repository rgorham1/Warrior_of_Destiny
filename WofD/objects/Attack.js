
export default class Attack 
{
	static getStandardApCost()
	{
		if (this === Attack)
		{
			// Error Type 2. Abstract methods can not be called directly.
      		throw new TypeError("Can not call static abstract method getStandardApCost.");
		}
		else if (this.getStandardApCost === Attack.getStandardApCost)
		{
			// Error Type 3. The child has not implemented this method.
      		throw new TypeError("Please implement static abstract method getStandardApCost.");
		}
		else{
			// Error Type 5. The child has implemented this method but also called `super.foo()`.
      		throw new TypeError("Do not call static abstract method getStandardApCost from child.");
		}

	}
    constructor() 
    {
        if (this.constructor === Attack) 
            throw new TypeError('Abstract class "Attack" cannot be instantiated directly.'); 
        //else (called from child)
    	// Check if all instance methods are implemented.
    	if (this.getStandardApCost === Attack.prototype.getStandardApCost) 
    	{
      		// Error Type 4. Child has not implemented this abstract method.
      		throw new TypeError("Please implement abstract method getStandardApCost.");
    	}

    	if (this.getStandardHpDmg === Attack.prototype.getStandardHpDmg) 
    	{
      		// Error Type 4. Child has not implemented this abstract method.
      		throw new TypeError("Please implement abstract method getStandardHpDmg.");
    	}
        
        if (this.getFriendlyName === Attack.prototype.getFriendlyName) 
    	{
      		// Error Type 4. Child has not implemented this abstract method.
      		throw new TypeError("Please implement abstract method getFriendlyName.");
    	}
    }

    // An abstract method.
  	getStandardApCost() 
  	{
    	// Error Type 6. The child has implemented this method but also called `super.foo()`.
    	throw new TypeError("Do not call abstract method foo from child.");
  	}

  	getStandardHpDmg() 
  	{
    	// Error Type 6. The child has implemented this method but also called `super.foo()`.
    	throw new TypeError("Do not call abstract method foo from child.");
  	}

  	getFriendlyName() 
  	{
    	// Error Type 6. The child has implemented this method but also called `super.foo()`.
    	throw new TypeError("Do not call abstract method foo from child.");
  	}



}
