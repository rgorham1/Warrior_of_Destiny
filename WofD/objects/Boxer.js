import PlayableCharacter from './PlayableCharacter';

class Boxer extends PlayableCharacter
{
	constructor()
	{
		super();
		this.attackList = new Map();
		this.attackList
		.set('Hidden Mist', 
		{ 
			apCost: 20,
			hpDmg: 18
		})
		.set('Blowgun Fury', 
		{ 
			apCost: 15,
			hpDmg: 12
		})
		.set('Silent Night with Stars', 
		{ 
			apCost: 27,
			hpDmg: 29
		});

		this.attrs = [88, 85, 3, 5, 2, 3, 2, 1, 'g', false, false];
	}

}