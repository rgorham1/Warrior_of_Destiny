import Attack from './Attack';

export default class Character
{
	constructor()
	{
		this.userName = null;
		this.level = null;
		this.xp = null;
		this.hitPoints = null;
		this.maxHitPoints = null;

		this.attackPoints = null;
		this.maxAttackPoints = null;

		this.inventory = null;
		this.attackList = null;
		this.aliveStatus = true;
		this.attrs = null;
		//this.weaponEquipped = null;
	}

	//general character info
	getInfo()
	{
		console.log('\nCharacter Info:\n\nName: this.userName\nLevel: this.level\nHP: this.hitPoints\n');
	}

	// getter functions
	getHP()
	{
		return this.hitPoints;
	} 

	getAp()
	{
		return this.attackPoints;
	}

	getUserName()
	{
		return this.userName;
	}

	getLevel()
	{
		return this.level;
	}
	
	getAliveStatus()
	{
		return this.aliveStatus;
	}
	
	getAttackList()
	{
		return this.attackList;
	}


	getChi()
	{
		//return chi;
	}

	getStrength()
	{
		//return strength;
	}
	
	getLuck()
	{
		//return luck;
	}
	
	getAttackList()
	{
		//return attackList;
	}

	//AP/HP related
	isCharacterDead(hpRemaining)
	{
		if(hpRemaining <= 0)
			this.aliveStatus = false;
	}
	/*

	//character actions
	//inventory related
	showInventory()
	{
		foreach($this->inventory as $key => $value)
		{
			if($value != null)
				echo "\n{$key}: {$value}\n";
			else
				echo "\n{$key}: EMPTY\n";
		}
	}
	
	public function getFirstFreeInventorySlot()
	{
		$idx = null;
		foreach($this->inventory as $key => $value)
		{
			if($value == null)
			{
				$idx = $key;
				break;
			}					
		}
		return $idx;  //if idx is null then player inventory is full
	}	
	
	addToInventory(Potion $item)
	{
		slot = this.getFirstFreeInventorySlot();
		if(slot == null)
		{
			console.log("No room in your inventory!");
			return false;
		}
		else
		{
			this.inventory[slot] = item;
			console.log("Item successfully added to inventory.");
			return true;
		}	
	}
	
	public function equip($invenSlot)
	{
		$selection = $this->inventory[$invenSlot];
		$type = get_class($selection);
		
		if($selection == null)
			echo "\nThat inventory slot is empty!\n";
		else if(!is_a($selection, "Weapon"))
			echo "\nYou can not equip a {$type} -- only weapons may be equipped.\n";
		else
			echo "\n{$type} now equiped.\n";
	}
	
	usePotion(callingPlayerHp)
	{
		hpToAdd = callingPayerHp * 0.07; //boots HP by 7% of players full HP
		return hpToAdd;
	}
	
	// TODO figure out what this does
	getApCost(attackArr, chosenAttack)
	{
		for (let attackOption of attackArr)            //this determines which attack the user actually choose 
		{														  //then gets the AP cost
			if(attackOption->getFriendlyName() == choosenAttack)
			{
				attackApCost = attackOption->getStandardApCost();
				return attackApCost;
			}
		}
		return -1;
	}
	
	getAttackDamage(attackArr, chosenAttack)
	{
		foreach(attackArr as attackOption)            //this determines which attack the user actually choose 
		{														  //then applies the correct damage to the enemy
			if(attackOption->getFriendlyName() == chosenAttack)
			{
				damageFromAttack = attackOption->getStandardHpDmg();
				return damageFromAttack;
			}
		}
		return -1;
	}
	*/

	takeHit(dmg)
	{
		hp = this.hitPoints - dmg;
		
		if(hp <= 0)
			this.hitPoints = 0;
		else
			this.hitPoints = hp;	
	}

	meetsApRequirement(moveApCost)
	{
		if(moveApCost <= this.attackPoints)
			return true;
			
		return false;
	}

	deductAp(apPoints)
	{
		newAp = (this.attackPoints - apPoints);
		if(newAp <= 0)
			this.attackPoints = 0;
		else
			this.attackPoints = newAp;
	}

	//show players attack list
	showAttackFriendlyName()
	{
		//console.log(Array.from(this.attackList.keys()).join(', '));
		for(let [key, value] of this.attackList)
		{
			console.log(key);
		}
		
	}
	
	//modify this function to pass in the users chi attribute 
	//evaluate the attribute to 
	giveBackAp(chiAttribute)
	{
		mutliplier;
		
		if(chiAttribute == 1)
			mutliplier = 0.01;
		else if(chiAttribute == 2)
			mutliplier = 0.02;
		else if(chiAttribute == 3)
			mutliplier = 0.03;
		else if(chiAttribute == 4)
			mutliplier = 0.04;
		else if(chiAttribute == 5)
			mutliplier = 0.05;
		else if(chiAttribute == 6)
			mutliplier = 0.06;
		else if(chiAttribute == 7)
			mutliplier = 0.07;
		else if(chiAttribute == 8)
			mutliplier = 0.08;
		else if(chiAttribute == 9)
			mutliplier = 0.09;
		else if(chiAttribute >= 10)
			mutliplier = 0.1;
			
		numOfAp = Math.ceil((this.maxAttackPoints * mutliplier));  //round up to the nearest integer
		
		if((this.attackPoints + numOfAp) > this.maxAttackPoints)
			this.attackPoints = this.maxAttackPoints;
		else 
			this.attackPoints = (this.attackPoints + numOfAp);
	}
	
	getCriticalHitMultiplier(strengthAttribute)
	{
		mutliplier = 0;
		
		if(strengthAttribute == 1)
			mutliplier = 0.01;
		else if(strengthAttribute == 2)
			mutliplier = 0.02;
		else if(strengthAttribute == 3)
			mutliplier = 0.03;
		else if(strengthAttribute == 4)
			mutliplier = 0.04;
		else if(strengthAttribute == 5)
			mutliplier = 0.05;
		else if(strengthAttribute == 6)
			mutliplier = 0.06;
		else if(strengthAttribute == 7)
			mutliplier = 0.07;
		else if(strengthAttribute == 8)
			mutliplier = 0.08;
		else if(strengthAttribute == 9)
			mutliplier = 0.09;
		else if(strengthAttribute >= 10)
			mutliplier = 0.1;
		
		return mutliplier;
	}
	
}
