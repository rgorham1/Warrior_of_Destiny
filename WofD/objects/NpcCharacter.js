import Character from './Character';

class NpcCharacter extends Character
{
	constructor(userName, hp, ap, attackList, attrs)
	{
		super();
		this.userName = userName;
		this.hitPoints = hp;
		this.maxHitPoints = hp;
		this.attackPoints = ap;
		this.maxAttackPoints = ap;
		this.aliveStatus = true;
		this.inventory = [null, null, null, null];
		this.attackList = [];
		this.attrs = [];
	}


}
/*
let n = new NpcCharacter('Bob', 100, 50, null, null);
console.log(n.userName);
if (n.getAliveStatus())
	console.log('its alive');
n.isCharacterDead(15);
console.log(n.getAliveStatus());
console.log(n.getHP());
n.takeHit(57);
console.log(n.getHP());
n.takeHit(57);
n.isCharacterDead(n.getHP());
console.log(n.getAliveStatus());
console.log(n.inventory.length);
*/