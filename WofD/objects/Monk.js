import PlayableCharacter from './PlayableCharacter';

class Monk extends PlayableCharacter
{
	constructor()
	{
		super();
		this.attackList = new Map();
		this.attackList
		.set('Flying Dragon Fists', 
		{ 
			apCost: 20,
			hpDmg: 10
		})
		.set('Shaolin Secret Move of Doom', 
		{ 
			apCost: 35,
			hpDmg: 20
		})
		.set('Bald Headed Eagle Strike', 
		{ 
			apCost: 25,
			hpDmg: 12
		});

		this.attrs = [80, 100, 2, 1, 3, 4, 5, 1, 'g', false, false];
	}

}



let m = new Monk();
m.showAttackFriendlyName();
