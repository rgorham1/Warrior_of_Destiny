import PlayableCharacter from './PlayableCharacter';

class Ninja extends PlayableCharacter
{
	constructor()
	{
		super();
		this.attackList = new Map();
		this.attackList
		.set('Hidden Mist', 
		{ 
			apCost: 20,
			hpDmg: 18
		})
		.set('Blowgun Fury', 
		{ 
			apCost: 15,
			hpDmg: 12
		})
		.set('Silent Night with Stars', 
		{ 
			apCost: 27,
			hpDmg: 29
		});

		this.attrs = [83, 83, 5, 1, 1, 1, 2, 6, 'e', false, false];
	}

}
