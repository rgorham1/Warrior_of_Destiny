import Attack from './Attack';

class FlyingDragonFists extends Attack
{
	constructor()
	{
		super();
	}

	getStandardApCost()
	{
		return 20;
	}
	
	getStandardHpDmg()
	{
		return 10;
	}

	getFriendlyName()
	{
		return "Flying Dragon Fists";
	}

}

// monk base attacks
class ShaolinSecretMoveOfDoom extends Attack
{
	constructor()
	{
		super();
	}

	getStandardApCost()
	{
		return 35;
	}
	
	getStandardHpDmg()
	{
		return 20;
	}

	getFriendlyName()
	{
		return "Shaolin Secret Move of Doom";
	}

}

class BaldHeadedEagleStrike extends Attack
{
	getStandardApCost()
	{
		return 25;
	}
	
	getStandardHpDmg()
	{
		return 12;
	}
	
	getFriendlyName()
	{
		return "Bald Headed Eagle Strike";
	}
}