Boss_Attacks = new Meteor.Collection( 'boss_character_attacks');

BossAtttacksSchema = new SimpleSchema({
	attack_name:
	{
		type: String
	},

	damage:
	{
		type: Number
	},

	cost:
	{
		type: Number
	},
	// character id from base character collection
	boss_name:
	{
		type: String
	}

});

Boss_Attacks.attachSchema(BossAtttacksSchema);


var boss_character_attack = [
  {
    "attack_name": "Haymaker of the Bishop",
    "damage": 15,
    "cost": 14,
    "boss_name": "Sir Joffrey"
  },
  {
    "attack_name": "The Mountain",
    "damage": 30,
    "cost": 27,
    "boss_name": "Sir Joffrey"
  },
  {
    "attack_name": "Kingslayer",
    "damage": 40,
    "cost": 40,
    "boss_name": "Sir Joffrey"
  },
  {
    "attack_name": "Starfish",
    "damage": 0,
    "cost": 55,
    "boss_name": "King Pies"
  },
  {
    "attack_name": "Yeti",
    "damage": 22,
    "cost": 20,
    "boss_name": "King Pies"
  },
  {
    "attack_name": "Abominable Snowman",
    "damage": 35,
    "cost": 30,
    "boss_name": "King Pies"
  },
  {
    "attack_name": "20 Cracks Choke",
    "damage": 25,
    "cost": 27,
    "boss_name": "Brazilian MotherChoker"
  },
  {
    "attack_name": "Hell''s Grip",
    "damage": 20,
    "cost": 15,
    "boss_name": "Brazilian MotherChoker"
  },
  {
    "attack_name": "Black Hole",
    "damage": 40,
    "cost": 40,
    "boss_name": "Brazilian MotherChoker"
  },
  {
    "attack_name": "1000 Sticks",
    "damage": 30,
    "cost": 30,
    "boss_name": "Commander Naked Head Basher"
  },
  {
    "attack_name": "Head of Rocks",
    "damage": 22,
    "cost": 20,
    "boss_name": "Commander Naked Head Basher"
  },
  {
    "attack_name": "The Big Stick",
    "damage": 37,
    "cost": 35,
    "boss_name": "Commander Naked Head Basher"
  },
  {
    "attack_name": "Delta Force Stirke",
    "damage": 40,
    "cost": 37,
    "boss_name": "Chuck Tyson"
  },
  {
    "attack_name": "Ear Ripper",
    "damage": 15,
    "cost": 12,
    "boss_name": "Chuck Tyson"
  },
  {
    "attack_name": "Texas Ranger Kick",
    "damage": 27,
    "cost": 25,
    "boss_name": "Chuck Tyson"
  },
  {
    "attack_name": "Boomerang from Down Under",
    "damage": 15,
    "cost": 17,
    "boss_name": "Mockodile Crundee"
  },
  {
    "attack_name": "They Call Me Machete",
    "damage": 20,
    "cost": 22,
    "boss_name": "Mockodile Crundee"
  },
  {
    "attack_name": "My Crocodile Friend",
    "damage": 45,
    "cost": 50,
    "boss_name": "Mockodile Crundee"
  },
  {
    "attack_name": "Sword of Fire",
    "damage": 30,
    "cost": 32,
    "boss_name": "Golden Samurai"
  },
  {
    "attack_name": "500 Slashes",
    "damage": 42,
    "cost": 40,
    "boss_name": "Golden Samurai"
  },
  {
    "attack_name": "Off With Your Head",
    "damage": 35,
    "cost": 35,
    "boss_name": "Golden Samurai"
  },
  {
    "attack_name": "Enter the Dragon",
    "damage": 35,
    "cost": 37,
    "boss_name": "Lei Bruce"
  },
  {
    "attack_name": "Green Hornet",
    "damage": 25,
    "cost": 27,
    "boss_name": "Lei Bruce"
  },
  {
    "attack_name": "The Last Dragon",
    "damage": 44,
    "cost": 45,
    "boss_name": "Lei Bruce"
  }
];

//boss_character_attack.forEach(doc =>{
//	Boss_Attacks.insert(doc);
//});
