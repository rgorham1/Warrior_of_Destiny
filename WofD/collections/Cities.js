Cities = new Meteor.Collection('cities');

CitiesSchema = new SimpleSchema({
	city:
	{
		type: String
	},

	continent:
	{
		type: String
	},

	style:
	{
		type: String,
		optional: true
	}
});

Cities.attachSchema(CitiesSchema);

var geography_city = [
  {
    "city": "Research Station Alpha",
    "continent": "Anartica",
    "style": "Genetics"
  },
  {
    "city": "Frozen Rock",
    "continent": "Anartica",
    "style": "Ice Style"
  },
  {
    "city": "Peter Island",
    "continent": "Anartica",
    "style": " "
  },
  {
    "city": "New Zealand",
    "continent": "Australia",
    "style": "Taiaha"
  },
  {
    "city": "Sydney",
    "continent": "Australia",
    "style": "Kangaroo Boxing"
  },
  {
    "city": "Tasmania",
    "continent": "Australia",
    "style": " "
  },
  {
    "city": "Okinawa",
    "continent": "Asia",
    "style": "Karate"
  },
  {
    "city": "Korea",
    "continent": "Asia",
    "style": "Tae KwonDo"
  },
  {
    "city": "India",
    "continent": "Asia",
    "style": "Gatka"
  },
  {
    "city": "Shanghai",
    "continent": "Asia",
    "style": "Wing Chun"
  },
  {
    "city": "Tokyo",
    "continent": "Asia",
    "style": "Aikido"
  },
  {
    "city": "Mt. Fuji",
    "continent": "Asia",
    "style": "Sumo"
  },
  {
    "city": "Malaysia",
    "continent": "Asia",
    "style": "Silat"
  },
  {
    "city": "Temple",
    "continent": "Asia",
    "style": "WuTang"
  },
  {
    "city": "Israel",
    "continent": "Asia",
    "style": "Krav Maga"
  },
  {
    "city": "Chinatown",
    "continent": "Asia",
    "style": "Tai Chi"
  },
  {
    "city": "Hong Kong",
    "continent": "Asia",
    "style": " "
  },
  {
    "city": "Beijing",
    "continent": "Asia",
    "style": " "
  },
  {
    "city": "South Africa",
    "continent": "Africa",
    "style": "Nguni Stick Fighting"
  },
  {
    "city": "Congo",
    "continent": "Africa",
    "style": "Mukumbusu"
  },
  {
    "city": "Zimbabwe",
    "continent": "Africa",
    "style": "Gwindulumutu"
  },
  {
    "city": "Liberia",
    "continent": "Africa",
    "style": " "
  },
  {
    "city": "Nigeria",
    "continent": "Africa",
    "style": "Dambe"
  },
  {
    "city": "Belgium",
    "continent": "Europe",
    "style": "Kickboxing"
  },
  {
    "city": "Paris",
    "continent": "Europe",
    "style": "Savate"
  },
  {
    "city": "London",
    "continent": "Europe",
    "style": " "
  },
  {
    "city": "Norway",
    "continent": "Europe",
    "style": "Stav"
  },
  {
    "city": "Switzerland",
    "continent": "Europe",
    "style": "Fencing"
  },
  {
    "city": "Siberia",
    "continent": "Europe",
    "style": "Systema"
  },
  {
    "city": "Scotland",
    "continent": "Europe",
    "style": "Shin Kicking"
  },
  {
    "city": "NYC",
    "continent": "North America",
    "style": "Wrestling"
  },
  {
    "city": "Alcatraz",
    "continent": "North America",
    "style": "Jailhouse Rock"
  },
  {
    "city": "Boston",
    "continent": "North America",
    "style": "Oom Yung Doe"
  },
  {
    "city": "West VA",
    "continent": "North America",
    "style": "Eye Gouging"
  },
  {
    "city": "Chicago",
    "continent": "North America",
    "style": "Street Fighting"
  },
  {
    "city": "NC",
    "continent": "North America",
    "style": " "
  },
  {
    "city": "Montreal",
    "continent": "North America",
    "style": "Okitchitaw"
  },
  {
    "city": "Las Vegas",
    "continent": "North America",
    "style": "The Cane As A Weapon"
  },
  {
    "city": "LA",
    "continent": "North America",
    "style": "Chun Kuk Do"
  },
  {
    "city": "Caritoba",
    "continent": "South America",
    "style": "Vale Tudo"
  },
  {
    "city": "Venezuela",
    "continent": "South America",
    "style": "Vacon"
  },
  {
    "city": "Argentina",
    "continent": "South America",
    "style": "Capoeira"
  },
  {
    "city": "Lima",
    "continent": "South America",
    "style": "Rumi Maki"
  },
  {
    "city": "Rio de Janeiro",
    "continent": "South America",
    "style": " "
  }
];


//geography_city.forEach(doc =>{
	//Cities.insert(doc);
	//});