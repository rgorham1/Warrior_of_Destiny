User_Character = new Meteor.Collection('user_character');

UserCharacterSchema = new SimpleSchema({
	user:
	{
		type: Number
	},

	character:
	{
		type: String
	},

	hp:
	{
		type: Number
	},

	ap:
	{
		type: Number
	},

	speed:
	{
		type: Number
	},

	strength:
	{
		type: Number
	},

	luck:
	{
		type: Number
	},

	intelligence:
	{
		type: Number
	},

	chi:
	{
		type: Number
	},

	stealth:
	{
		type: Number
	},

	attack_1:
	{
		type: Number
	},

	attack_2:
	{
		type: Number
	},

	attack_3:
	{
		type: Number
	},

	xp:
	{
		type: Number
	}
});

User_Character.attachSchema(UserCharacterSchema);