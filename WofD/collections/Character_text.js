Character_text = new Mongo.Collection('character_text');


character_text_schema = new SimpleSchema({
	character:
	{
		type: String
	},
	fight_begin_text:
    {
        type: String,
        optional: true
    },
    fight_win_text_text:
    {
        type: String,
        optional: true
    },
    fight_lose_text_text:
    {
        type: String,
        optional: true
    },
    bio:
    {
        type: String,
        optional: true
    }
});

var character_text = [
  {
    "character": "Boxer",
    "fight_begin_text": "Im going to bend you in half.",
    "fight_win_text": "You went night night.",
    "fight_lose_text": "I should have ran like Mayweather.",
    "bio": "A mild mannered man with a squeaky voice but once he starts fighting becomes a beast."
  },
  {
    "character": "BJJ",
    "fight_begin_text": "Let me know what you dreamed about after you wake up.",
    "fight_win_text": "All men go to sleep.",
    "fight_lose_text": "I need to learn how to strike.",
    "bio": "Grew up in the slums of Brazil and looking for an outlet turned to JiuJitsu. As a way to escape the violence that surrounded him, he let BJJ consume him."
  },
  {
    "character": "Ninja",
    "fight_begin_text": "Prepare for the silent death.",
    "fight_win_text": "How you can beat a shadow?",
    "fight_lose_text": "I didnt think you could saw me.",
    "bio": "Stealthy assassin trained in the art of deception. It is said that this purveyor of darkness controls the ancient elements."
  },
  {
    "character": "Muay Thai",
    "fight_begin_text": "I dont break.",
    "fight_win_text": "You break.",
    "fight_lose_text": "I have failed the ancient.",
    "bio": "Abandoned in a beach paradise somewhere in Thailand, grew up to be anything but soft like the sand that surrounded him. Has a body as solid as the sacred elephant. Have limbs as hard as pure steel."
    
  },
  {
    "character": "Sambo",
    "fight_begin_text": "Prepare for pounding.",
    "fight_win_text": "Nothing can stop me not even the cold.",
    "fight_lose_text": "I must be getting old.",
    "bio": "Frigid hulk of a man. Can control the ice and withstand radiation after surviving Chernobyl. Bent on path of destruction since losing family in Chernobyl incident. Blames the world."
  },
  {
    "character": "Commander Naked Head Basher",
    "fight_begin_text": "My stick is bigger than yours.",
    "fight_win_text": "Did you hear the voices also?",
    "fight_lose_text": "The devil made me do it.",
    "bio": null
    
  },
  {
    "character": "King Pies",
    "fight_begin_text": "yel",
    "fight_win_text": "whewee",
    "fight_lose_text": "owah",
    "bio": null
    
  },
  {
    "character": "Chuck Tyson",
    "fight_begin_text": "Dont let me see your children.",
    "fight_win_text": "I was a POW and stopped an invasion by myself. What did you do?",
    "fight_lose_text": "Oh well",
    "bio": null   
  },
  {
    "character": "Mockodile Crundee",
    "fight_begin_text": "Aye mate say hello to my little friend",
    "fight_win_text": "You cant beat a crocodile.",
    "fight_lose_text": "Good fight mate!",
    "bio": null
    
  },
  {
    "character": "Sir Joffrey",
    "fight_begin_text": "I will have your head on a spike.",
    "fight_win_text": "Time to see what your head looks like on a spike.",
    "fight_lose_text": "I will have your head on a spike.",
    "bio": null
    
  },
  {
    "character": "Golden Samurai",
    "fight_begin_text": "I fight for the honor of myself.",
    "fight_win_text": "Honor is the greatest motivator.",
    "fight_lose_text": "I lost my honor. I will kill myself now.",
    "bio": null
    
  },
  {
    "character": "Lei Bruce",
    "fight_begin_text": "You must be like water.",
    "fight_win_text": "Boards dont hit back",
    "fight_lose_text": "Even in defeat I still win.",
    "bio": null
    
  },
  {
    "character": "Monk",
    "fight_begin_text": "You are going to learn today.",
    "fight_win_text": "Time is the greatest teacher.",
    "fight_lose_text": "My bones ache.",
    "bio": "A wise old man who has studied the Shaolin style for as long as he could remember. He may be small in stature but he is big in wisdom."
  },
  {
    "character": "MMA",
    "fight_begin_text": "You cant beat someone trained in everything.",
    "fight_win_text": "How did my foot taste?",
    "fight_lose_text": "I will continue my journey to master everything.",
    "bio": "Looking to master all styles to become best fighter.  All around good fighter,  well versed in various techniques and styles."
  }
];
/*
character_text.forEach(doc =>{
	Character_text.insert(doc);
});
*/

